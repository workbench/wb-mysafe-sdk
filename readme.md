# Mysafe Node.js SDK

## Installation

`npm i gitlab:workbench/wb-mysafe-sdk`

## Usage
**If no configuration is supplied, the SDK will connect to the sandbox environment (not suitable for production use)**

```
const MysafeSDK = require('wb-mysafe-sdk')
const mysafe = new MysafeSDK()

mysafe.status()
  .then(() => console.log('mysafe connection successful'))
```

## API

After installation, documentation is available in the following location:

`/node_modules/wb-mysafe-sdk/docs/index.html`