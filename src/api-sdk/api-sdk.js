const Internal = require('./src/api-internal.js')

const $ = Symbol()

class MysafeSDK {
  /**
   * SDK Configuration Options
   * @typedef {object} Config
   * @property {string} appSecretKey - App secret key in base64 format, defaults to process.env.APP_SECRET_KEY
   * @property {string} auditPublicKeyFile - Location of the audit public key used to create snapshots, defaults to process.env.AUDIT_PUBLIC_KEY_FILE
   * @property {string} auditSecretKeyFile - Location of the encrypted audit secret key used to decrypt snapshots, defaults to process.env.AUDIT_SECRET_KEY_FILE
   * @property {string} mysafeEndpoint - Mysafe API endpoint, defaults to process.env.MYSAFE_ENDPOINT
   * @property {string} mysafeApiKey - Mysafe API key, defaults to process.env.MYSAFE_APIKEY (optional, may also be supplied per SDK call)
   * @property {GreenIdConfig} greenId - GreenID configuration options
   */
  
  /**
   * GreenID Configuration Options
   * @typedef {object} GreenIdConfig
   * @property {string} endpoint - Endpoint for GreenID API
   * @property {string} accountId - GreenID Account ID
   * @property {string} password - GreenID API Password
   */
  
  /**
   * Defines a set of data using heirarchical, dot-notation.
   * 
   * Examples:
   * ```
   *   'name'  =>  { name: { first: 'John', last: 'Smith' } }
   *
   *   'name.first'  =>  { name: { first: 'John' } }
   * 
   *   '' or null  =>  (all data)
   * ```
   * @typedef {string} ObjectKey
   */
  
  /**
   * Feed History Item
   * @typedef {object} FeedHistoryItem
   * @property {string} key - The object key corresponding to this data item
   * @property {string} value - String representation of the data item's value
   * @property {number} timestamp - The time at which this data item was published
   * @property {string} dataHmac - Integrity check for this data item's value which can be used to track changes in revoked or prior data
   * @property {string} error - Either null, 'decryption failed' if the correct key was not available (revoked) or 'integrity check failed' (data may be corrupt)
   */
  
  /**
   * Snapshot
   * @typedef {object} Snapshot
   * @property {number} timestamp - The time at which the snapshot was created
   * @property {object} data - The full data set at the time of the snapshot
   */
   
  /**
   * Verification Result
   * @typedef {object} VerificationResult
   * @property {string} status - The status of the verification as per greenID status codes https://vixverify.atlassian.net/wiki/spaces/GREEN/pages/60482483/Reference+Tables+V3#ReferenceTablesV3-OverallOutcomeStates
   * @property {string} verificationId - The verificationId that can be used to perform subsequent verification status checks using checkVerification
   */
  
  /**
   * Creates a new SDK instance
   * @param {Config} options - Configuration Options for the SDK. If omitted, MysafeSDK will connect to the sandbox environment.
   */
  constructor (options) {
    this[$] = new Internal(options)
  }

  /**
   * Performs a connectivity test with the Mysafe API
   * @returns {Promise<void>} - A promise that fulfills on success
   */
  status () { return this[$].status() }
  
  /**
   * Saves a subscription, completing the authorization process
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} subscription - The encrypted set of identifiers & keys required to interact with the subscription
   * @param {string} userSecret - An encrypted, ephemeral secret key used to lock specific actions to user sessions
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<void>} - A promise that fullfills on success
   */
  subscribe (subscriptionId, subscription, userSecret, apiKey) { return this[$].subscribe(...arguments) }
  
  /**
   * Executes a query against the given subscription
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {Array<ObjectKey>} keys - The object keys to return (optional - will return all data if not specified)
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<object>} - A promise that fulfills with the requested data
   */
  query (subscriptionId, keys, apiKey) { return this[$].query(...arguments) }
  
  /**
   * Executes a query against the given subscription, returning all historical values
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {Array<ObjectKey>} keys - The object keys to return (optional - will return all data if not specified)
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<Array<FeedHistoryItem>>} - A promise that fulfills with the requested data
   */
  history (subscriptionId, keys, apiKey) { return this[$].history(...arguments) }
  
  /**
   * Revokes access to a given permission subset (as defined in Subscription.permissionKeys)
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} name - The name of the subset to revoke
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<void>} - A promise that fullfills on success
   */
  revoke (subscriptionId, name, apiKey) { return this[$].revoke(...arguments) }
  
  /**
   * Revokes access to all data
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<void>} - A promise that fullfills on success
   */
  revokeAll (subscriptionId, apiKey) { return this[$].revokeAll(...arguments) }
  
  /**
   * Creates and saves a secure snapshot of the complete data set
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<void>} - A promise that fullfills on success
   */
  createSnapshot (subscriptionId, apiKey) { return this[$].createSnapshot(...arguments) }
  
  /**
   * Retrieves and decrypts all snapshots for the given subscription
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} dateSuffix - The date prefix to return (e.g. '2017', '2017-01', '2017-01-01')
   * @param {string} auditPassword - Password required to decrypt the audit secret key (**PERSISTENT STORAGE OF THIS PASSWORD PROHIBITED**)
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<Array<Snapshot>>} - A promise that fullfills with the requested snapshots
   */
  retrieveSnapshots (subscriptionId, datePrefix, auditPassword, apiKey) { return this[$].retrieveSnapshots(...arguments) }
  
  /**
    * Verifies a customer's identity using identity documents on file
    * @param {string} subscriptionId - The unique identifier of the subscription
    * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
    * @returns {Promise<VerificationResult>} - The verification result
    */
  verify (subscriptionId, apiKey) { return this[$].verify(...arguments) }

  /**
   * Checks the status of a verification
   * @param {string} subscriptionId - The unique identifier of the subscription
   * @param {string} verificationId - The unique identifier of the verification
   * @param {string} apiKey - Mysafe API key (optional - see SDK constructor)
   * @returns {Promise<VerificationResult>} - The verification result
   */
  checkVerification (subscriptionId, verificationId, apiKey) { return this[$].checkVerification(...arguments) }
  
  putBlob (subscriptionId, blobId, data, leaseExpiry, apiKey) { return this[$].putBlob(...arguments) }
  
  getBlob (subscriptionId, blobId, userSecret, apiKey) { return this[$].getBlob(...arguments) }
  
  createLease (subscriptionId, blobId, userSecret, leaseExpiry, apiKey) { return this[$].createLease(...arguments) }
  
  getLease (subscriptionId, leaseKey, apiKey) { return this[$].getLease(...arguments) }
}

module.exports = MysafeSDK
