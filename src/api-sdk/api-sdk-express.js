const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const SDK = require('./api-sdk')
const error = require('../helpers').expressError

SDK.server = (options) => {
  const sdk = new SDK(options)
  const app = express()
  const router = express.Router()

  app.use(bodyParser.urlencoded({extended: true}))
  app.use(bodyParser.json())
  app.use(bodyParser.raw({type: 'application/base64', limit: '10mb'}))
  app.use(cors({origin: true, credentials: true}))

  router.route('/ping')
    .get((req, res) => {
      if (req.headers['x-apigateway-event']) res.status(200).send(JSON.parse(decodeURIComponent(req.headers['x-apigateway-event'])).headers)
      else res.status(200).send(req.headers)
    })

  router.route('/status')
    .get((req, res) =>
      sdk.status()
        .then(() => res.sendStatus(200))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId')
    .put((req, res) =>
      sdk.subscribe(req.params.subscriptionId, req.body.subscription, req.headers.usersecret, req.headers.apikey)
        .then(() => res.sendStatus(200))
        .catch(error(res))
    )
    .get((req, res) =>
      sdk.query(req.params.subscriptionId, req.query.keys && req.query.keys.split(','), req.headers.apikey)
        .then(response => res.json(req.query.extended ? response : response.data))
        .catch(error(res))
    )
    .delete((req, res) =>
      sdk.revokeAll(req.params.subscriptionId, req.headers.apikey)
        .then(() => res.sendStatus(200))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/subsets/:subset')
    .delete((req, res) =>
      sdk.revoke(req.params.subscriptionId, req.params.subset, req.headers.apikey)
        .then(() => res.sendStatus(200))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/history')
    .get((req, res) =>
      sdk.history(req.params.subscriptionId, req.query.keys && req.query.keys.split(','), req.headers.apikey)
        .then(response => res.json(response))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/verifications')
    .post((req, res) =>
        sdk.verify(req.params.subscriptionId, req.headers.apikey)
          .then(response => res.json(response))
          .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/verifications/:verificationId')
    .get((req, res) =>
      sdk.checkVerification(req.params.subscriptionId, req.params.verificationId, req.headers.apikey)
        .then(response => res.json(response))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/snapshots')
    .post((req, res) =>
      sdk.createSnapshot(req.params.subscriptionId, req.headers.apikey)
        .then(() => res.sendStatus(200))
        .catch(error(res))
    )
    .get((req, res) =>
      sdk.retrieveSnapshots(req.params.subscriptionId, req.query.datePrefix, req.headers.auditpassword, req.headers.apikey)
        .then(response => res.json(response))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/blobs/:blobId')
    .put((req, res) => {
      if(req.headers['content-type'] !== 'application/base64') {
        return res.status(400).send('Incorrect content type, only application/base64 supported');
      }

        return sdk.putBlob(req.params.subscriptionId, req.params.blobId, req.body, req.headers.leaseexpiry, req.headers.apikey)
          .then(response => res.json(response))
          .catch(error(res))
      }
    )
    .get((req, res) =>
      sdk.getBlob(req.params.subscriptionId, req.params.blobId, req.headers.usersecret, req.headers.apikey)
        .then(response => response ? res.status(200).type('application/base64').send(response).end() : res.sendStatus(404))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/blobs/:blobId/leases')
    .post((req, res) =>
      sdk.createLease(req.params.subscriptionId, req.params.blobId, req.headers.usersecret, req.headers.leaseexpiry, req.headers.apikey)
        .then(response => res.json(response))
        .catch(error(res))
    )

  router.route('/subscriptions/:subscriptionId/blob-leases/:leaseKey')
    .get((req, res) =>
      sdk.getLease(req.params.subscriptionId, req.params.leaseKey, req.headers.apikey)
        .then(response => response ? res.status(200).type('application/base64').send(response).end() : res.sendStatus(404))
        .catch(error(res))
    )

  app.use(router)

  return app
}

module.exports = SDK
