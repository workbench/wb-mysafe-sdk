const fs = require('fs')
const { Peer, util } = require('../../mysafe-protocol')
const { encoding, Identity, KeyCollection, document, core } = require('../../crypto')
const { t64, f64, fasc, tasc, serialize, deserialize } = encoding
const http = require('axios')
const moment = require('moment')
const greenId = require('../../service-greenid')
const defaultConfig = require('./default-config')
const assert = require('assert')

const f64f = str => new Uint8Array(Buffer.from(str, 'base64'))
const t64f = arr => new Buffer(arr).toString('base64')

class Internal extends Peer {
  constructor (options) {
    super(options)
    this.config = Object.assign(defaultConfig, options)
    this.config.greenId = this.config.greenId || {}
  }
  
  init() {
    if (!this.id) {
      this.id = new Identity(process.env.APP_SECRET_KEY || this.config.appSecretKey)
      this.appId = this.id.fingerprint()
      this.mysafeEndpoint = /*process.env.MYSAFE_ENDPOINT ||*/ this.config.mysafeEndpoint
      this.apiKey = process.env.MYSAFE_APIKEY || this.config.mysafeApiKey
      this.auditPublic = new Identity(
        fs.readFileSync(process.env.AUDIT_PUBLIC_KEY_FILE || this.config.auditPublicKeyFile, 'utf8')
      )
      this.auditSecretKeyData = fs.readFileSync(process.env.AUDIT_SECRET_KEY_FILE || this.config.auditSecretKeyFile, 'utf8')
      this.greenId = {
        endpoint: process.env.GREENID_ENDPOINT || this.config.greenId.endpoint,
        accountId: process.env.GREENID_ACCOUNTID || this.config.greenId.accountId,
        password: process.env.GREENID_PASSWORD || this.config.greenId.password
      }
    }
  }
  
  getApiKey(extApiKey) {
    //TEMPORARY workaround for old api key - DO NOT publish
    if (extApiKey === 'JrFSD9yeD-UqK5hN4bnoansiaWQiOiJXSFVNUmwtc05STmI1TUdyblhOLUp3Iiwic2NvcGUiOiJhcHAifQ')
      return this.apiKey
    
    return extApiKey || this.apiKey
  }

  status() {
    this.init()
    return http.get(this.mysafeEndpoint + '/ping')
  }
  
  query (subscriptionId, keys, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.getSubscription(subscriptionId, apiKey)
      .then(subscription => {
        const query = {
          keys: keys ? util.hmacPath(keys, subscription.metaKey) : [ '' ],
          history: false
        }
      
        return http.get(this.mysafeEndpoint + '/feed/' + subscription.feedId + '/' + encodeURIComponent(JSON.stringify(query)), { headers: { apiKey } })
          .then(response => {
            for (let keyId of Object.keys(response.data.content.deviceKeys)) {
              subscription.addKey(response.data.content.deviceKeys[keyId])
            }
            return this.decryptFeedState(subscription, response.data.content.feed)
          })
      })
  }
  
  history (subscriptionId, keys, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.getSubscription(subscriptionId, apiKey)
      .then(subscription => {      
        const query = {
          keys: keys ? util.hmacPath(keys, subscription.metaKey) : [ '' ],
          history: true
        }
      
        return http.get(this.mysafeEndpoint + '/feed/' + subscription.feedId + '/' + encodeURIComponent(JSON.stringify(query)), { headers: { apiKey } })
          .then(response => {
            for (let keyId of Object.keys(response.data.content.deviceKeys)) {
              subscription.addKey(response.data.content.deviceKeys[keyId])
            }
            return this.decryptFeedHistory(subscription, response.data.content.feed)
          })
      })
  }
  
  subscribe (subscriptionId, subscription, userSecret, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return Promise.resolve().then(() => {      
        if (userSecret) {
          const userId = new Identity(this.id.decryptPrivate(userSecret))
          subscription = userId.decryptSymmetric(subscription)
          subscription.publicKey = userId.exportPublicKey()
        } else {
          subscription = deserialize(subscription)
        }
      
        if (!subscription.feedKey instanceof Uint8Array) throw new Error('Invalid subscription')
        if (subscription.id !== subscriptionId) throw new Error('Invalid subscriptionId')
      
        const { id, feedKey, publicKey, permissionKeys } = subscription
      
        return Promise.all([
          this.set(`subscriptions.${id}`, serialize({ feedKey, publicKey }), null, apiKey),
          ...Object.keys(permissionKeys).map(name => this.saveKey(id, name, permissionKeys[name], apiKey))
        ])
    })
  }
  
  putBlob (subscriptionId, blobId, data, leaseExpiry, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    data = f64f(data.toString('utf8'))
    
    return Promise.all([
      http.request({
        url: this.mysafeEndpoint + `/apps/${this.appId}/blobs/${blobId}`,
        method: 'put',
        data: this.id.encryptSymmetric(data, true),
        headers: { 'content-type': 'text/plain', apiKey, leaseExpiry: leaseExpiry || '' }
      }),
      this.get(`subscriptions.${subscriptionId}`, apiKey)
    ])
      .then(result => {      
        const { blobKey, leaseKey } = result[0].data
        const userId = new Identity(deserialize(result[1]).publicKey)
        
        return this.set(`subscriptions.${subscriptionId}.keys.blob-${blobId}`, userId.encryptPublic(blobKey), null, apiKey)
          .then(() => ({ leaseKey }))
      })
  }
  
  getBlob (subscriptionId, blobId, userSecret, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.get(`subscriptions.${subscriptionId}.keys.blob-${blobId}`)
      .then(result => {
        if (!result) return null
      
        const userId = new Identity(this.id.decryptPrivate(userSecret))
        const blobKey = userId.decryptPrivate(result)
        
        return http.get(this.mysafeEndpoint + `/apps/${this.appId}/blobs/${blobId}`, { headers: { apiKey, blobKey } })
      })
      .then(response => response && t64f(this.id.decryptSymmetric(response.data, true)))
  }
  
  createLease (subscriptionId, blobId, userSecret, leaseExpiry, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.get(`subscriptions.${subscriptionId}.keys.blob-${blobId}`)
      .then(result => {
        const userId = new Identity(this.id.decryptPrivate(userSecret))
        const blobKey = userId.decryptPrivate(result)
        return http.request({
          url: this.mysafeEndpoint + `/apps/${this.appId}/blobs/${blobId}/leases`,
          method: 'post',
          headers: { apiKey, blobKey, leaseExpiry }
        })
      })
      .then(response => response.data)
  }
  
  getLease (subscriptionId, leaseKey, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.get(this.mysafeEndpoint + `/apps/${this.appId}/blob-leases/${leaseKey}`, { headers: { apiKey } })
      .then(response => response.data && t64(this.id.decryptSymmetric(response.data, true)))
  }

  saveKey (subscriptionId, name, key, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.set(`subscriptions.${subscriptionId}.keys.${name}`, t64(key), null, apiKey)
  }

  getSubscription (subscriptionId, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    const root = `subscriptions.${subscriptionId}`

    return this.getAll(root, apiKey)
      .then(results => {
        if (!results[root]) throw new Error('Subscription does not exist')
        const sub = Object.assign(new KeyCollection({ name: 'sdk' }), deserialize(results[root]))
        Object.assign(sub, util.expandFeedKey(sub.feedKey))
        sub.feedId = util.keyId(sub.feedKey)
        sub.subscriptionId = subscriptionId
        sub.addKey(sub.feedKey)
        Object.keys(results).filter(x => x !== root && results[x]).map(x => sub.addKey(results[x]))
        return sub
      })
  }

  revoke (subscriptionId, name, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.delete(`subscriptions.${subscriptionId}.keys.${name || 'default'}`, apiKey)
  }
  
  revokeAll (subscriptionId, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.deleteAll(`subscriptions.${subscriptionId}.keys`, apiKey)
  }
  
  createSnapshot (subscriptionId, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    const dateKey = moment().format('YYYYMMDDhhmmssSSS')
    
    return this.query(subscriptionId, null, apiKey)
      .then(result => this.set(`subscriptions.${subscriptionId}.snapshots.${dateKey}`, this.auditPublic.encryptPublic(result.data), null, apiKey))
  }
  
  retrieveSnapshots (subscriptionId, datePrefix, auditPassword, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    let auditSecret;
    try {
      auditSecret = new Identity(this.auditSecretKeyData, auditPassword)
    } catch(e) { }
    
    if (!auditSecret) return Promise.reject('Invalid audit password')
    
    return this.getAll(`subscriptions.${subscriptionId}.snapshots.${datePrefix || ''}`, apiKey)
      .then(snapshots => {
        const result = []
        for (let key of Object.keys(snapshots).sort()) {
          result.push({ 
            timestamp: moment(key.split('.').pop(), 'YYYYMMDDhhmmssSSS').valueOf(), 
            data: auditSecret.decryptPrivate(snapshots[key])
          })
        }
        return result
      })
  }

  delete (key, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.request({
      url: this.mysafeEndpoint + `/apps/${this.appId}/data/${key}`,
      method: 'delete',
      headers: { apiKey }
    })
  }
  
  deleteAll (key, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.request({
      url: this.mysafeEndpoint + `/apps/${this.appId}/data/${key}?startsWith=true`,
      method: 'delete',
      headers: { apiKey }
    })
  }
  
  set (key, value, expiry, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.request({
      url: this.mysafeEndpoint + `/apps/${this.appId}/data/${key}`,
      method: 'put',
      data: this.id.encryptSymmetric(value),
      headers: { 'content-type': 'text/plain', apiKey, expiry }
    })
  }

  get (key, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.get(this.mysafeEndpoint + `/apps/${this.appId}/data/${key}`, { headers: { apiKey } })
      .then(response => response.data && this.id.decryptSymmetric(response.data))
  }

  getAll (prefix, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return http.get(this.mysafeEndpoint + `/apps/${this.appId}/data/${prefix}?startsWith=true`, { headers: { apiKey } })
      .then(response => {
        Object.keys(response.data).map(k => { response.data[k] = this.id.decryptSymmetric(response.data[k]) })
        return response.data
      })
  }
  
  verify (subscriptionId, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return this.query(subscriptionId, null, apiKey)
      .then(userData =>
        greenId.beginVerification(userData, this.greenId)
          .then(result => greenId.completeVerification(userData, result.verificationId, this.greenId))
      )
  }

  checkVerification (subscriptionId, verificationId, apiKey) {
    this.init()
    apiKey = this.getApiKey(apiKey)
    
    return greenId.checkVerification(verificationId, this.greenId)
  }
}

module.exports = Internal