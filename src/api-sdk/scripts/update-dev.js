const spawn = require('child_process').spawn
const pkg = require('../package.json')

const args = [
  'update', 
  '--config', './dev-lambda-config.json',
  '--handler', 'lambda.handler',
  '--version', pkg.version.replace(/\./g, '_'),
  '--use-local-dependencies',
  '--cache-api-config', 'config',
  '--keep'
]
const child = spawn('claudia', args, { shell: true, stdio: 'inherit' })

child.on('message', data => console.log(data.toString()))
child.on('error', data => console.error(data.toString()))