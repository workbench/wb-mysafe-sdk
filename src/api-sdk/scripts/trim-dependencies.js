const fs = require('fs-extra')
const path = require('path')
const root = path.join(__dirname, '../node_modules')

fs.removeSync(root + '/aws-sdk')
fs.removeSync(root + '/ntru/dist/ntru.debug.js')
fs.removeSync(root + '/ntru/dist/ntru.debug.js.map')
fs.removeSync(root + '/axios/dist')
fs.removeSync(root + '/moment/locale')
fs.removeSync(root + '/moment/min')
fs.removeSync(root + '/moment/src')