const spawn = require('child_process').spawn
const pkg = require('../package.json')

const args = [ 
  'create',
  '--config', './prd-lambda-config.json',
  '--name', 'prd-mysafe-sdk',
  '--region', 'ap-southeast-2', 
  '--deploy-proxy-api',
  '--handler', 'lambda.handler',
  '--version', pkg.version.replace(/\./g, '_'),
  '--use-local-dependencies',
  '--cache-api-config', 'config',
  '--timeout', '30',
  '--keep'
]
const child = spawn('claudia', args, { shell: true, stdio: 'inherit' })

child.on('message', data => console.log(data.toString()))
child.on('error', data => console.error(data.toString()))