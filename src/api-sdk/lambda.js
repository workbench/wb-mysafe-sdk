const awsServerlessExpress = require('aws-serverless-express')
const route = require('./').server()
const server = awsServerlessExpress.createServer(route)
module.exports.handler = (event, context) => awsServerlessExpress.proxy(server, event, context)
