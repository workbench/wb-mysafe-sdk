const sourceMap = require('./src/source-map.json')
const xmlBuilder = require('./src/xml-builder.js')
const xml2js = require('xml2js')
const http = require('axios')
let Promise = require('bluebird')
const { getMappedData } = require('./src/verification-map.js')

const greenIdServiceEndpoint = 'https://test-au.vixverify.com/Registrations-Registrations/DynamicFormsServiceV3'

class GreenId {
  
  static getPermissions (sources) {
    return Object.keys(sources.map(source => sourceMap[source])
      .filter(m => !!m)
      .reduce((a, v) => a.concat(v),[])
      .reduce((a, v) => a[v] = 1 && a, {}))
  }

  static mapDataToVerification (data) {
    return getMappedData(data)
  }

  static beginVerification (queryResult, config) {
    const url = config.endpoint;
    let xml = new xmlBuilder(config.accountId, config.password);
    const body = xml.registerVerification(queryResult.data);

    const options = {headers: {'Content-Type': 'text/xml', 'Accept': 'text/xml'}};
    return http.post(url + `?registerVerification`, body, options)
      .then(response => {
        return new Promise((resolve,reject) =>{
          xml2js.parseString(response.data, function (err, result) {
            const verificationId = result['env:Envelope']['env:Body'][0]['ns2:registerVerificationResponse'][0].return[0].verificationResult[0].verificationId[0]
            const body = xml.getSources(verificationId);
            return http.post(url + `?getSources`, body, options)
              .then(response => {
                xml2js.parseString(response.data, function (err, result) {
                  const sources = result['env:Envelope']['env:Body'][0]['ns2:getSourcesResponse'][0].return[0].sourceList[0].source
                  let promiseArray = [];
                  sources.forEach((source) => {
                    const body = xml.getFields(verificationId, source.name[0]);
                    promiseArray.push(http.post(url + `?getFields`, body, options))
                  })
                  return Promise.all(promiseArray)
                    .then(result => {
                      let endResult = {
                        verificationId,
                        permissions: []
                      };
                      let sources = [];
                      result.forEach((res,index) => {
                        xml2js.parseString(res.data, function (err, result) {
                          const source = result['env:Envelope']['env:Body'][0]['ns2:getFieldsResponse'][0].return[0].sourceList[0].source[index].name[0]
                          sources[source] = result['env:Envelope']['env:Body'][0]['ns2:getFieldsResponse'][0].return[0].sourceFields[0].fieldList[0].sourceField
                        })
                      });
                      let sourceArray = []
                      for (let key in sources) {
                        if (sources.hasOwnProperty(key)) {
                          sourceArray.push(key)
                        }
                      }
                      endResult.permissions = GreenId.getPermissions(sourceArray);
                      resolve(endResult)
                    })
                })
              })
          })
        })
        
      })
      .catch(err => {
        err = err.response.data.split('<faultstring>').pop().split('</faultstring>').shift()
        console.error('Register verification error', err)
        return Promise.reject(err)
      })

  }
  
  static completeVerification (queryResult, verificationId, config) {
    const xml = new xmlBuilder(config.accountId, config.password);
    const url = config.endpoint;
    const vd = GreenId.mapDataToVerification(queryResult.data);
    const options = {headers: {'Content-Type': 'text/xml', 'Accept': 'text/xml'}};
    return Promise.reduce(vd, (total, source) => {
      let sourceKey = '';
      for (let key in source) {
        if (source.hasOwnProperty(key)) {
          sourceKey = key;
        }
      }
      const body = xml.setFields(verificationId, sourceKey, source[sourceKey]);
      return http.post(url + `?setFields`, body, options).then((contents) => {
        total.push(contents.data);
        return total
      })
      .catch(err => {
        err = err.response.data.split('<faultstring>').pop().split('</faultstring>').shift()
        console.error('Complete verification error', err)
        return Promise.reject(err)
      });
    }, []).then(() => GreenId.checkVerification(verificationId, config))
  }
  
  static checkVerification (verificationId, config) {
    const xml = new xmlBuilder(config.accountId, config.password);
    const url = config.endpoint;
    const body = xml.getVerificationResult(verificationId);
    const options = {headers: {'Content-Type': 'text/xml', 'Accept': 'text/xml'}};
    return http.post(url + `?getVerificationResult`, body, options)
      .then(response => {
        let status = '';
        xml2js.parseString(response.data, function (err, result) {
          status = result['env:Envelope']['env:Body'][0]['ns2:getVerificationResultResponse'][0].return[0].verificationResult[0].overallVerificationStatus[0]
        })
        return { status, verificationId }
      })
      .catch(err => {
        console.error('Check verification error', err.response.data)
        return Promise.reject(err)
      });
  }
}

module.exports = GreenId
