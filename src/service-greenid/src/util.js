class Util {
  static mapState(state) {
    if (!state) return null
    return ({
      'australian capital territory': 'ACT',
      'new south wales': 'NSW',
      'queensland': 'QLD',
      'south australia': 'SA',
      'tasmania': 'TAS',
      'victoria': 'VIC',
      'western australia': 'WA',
    })[state.toLowerCase()] || state
  }
  
  static mapCountry(country) {
    if (!country) return null
    return ({
      'australia': 'AU'
    })[country.toLowerCase()] || country
  }
}

module.exports = Util