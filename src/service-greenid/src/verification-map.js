const { mapState } = require('./util.js')

class VerificationMap {
  static getMappedData (data) {
    let verificationData = []

    if (data.identity.driversLicense) {
      const idName = data.identity.driversLicense.name || { }
      const state = mapState(data.identity.driversLicense.state).toLowerCase()

      verificationData.push({
        [`${state}regodvs`]: {
          [`greenid_${state}regodvs_number`]: data.identity.driversLicense.number,
          [`greenid_${state}regodvs_givenname`]: idName.first || data.name.first,
          [`greenid_${state}regodvs_middlename`]: idName.middle || data.name.middle || '',
          [`greenid_${state}regodvs_surname`]: idName.last || data.name.last,
          [`greenid_${state}regodvs_dob`]: data.dateOfBirth,
          [`greenid_${state}regodvs_tandc`]: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    if (data.identity.passport) {
      const idName = data.identity.passport.name || { }

      verificationData.push({
        passportdvs: {
          greenid_passportdvs_givenname: idName.first || data.name.first,
          greenid_passportdvs_middlename: idName.middle || data.name.middle || '',
          greenid_passportdvs_surname: idName.last || data.name.last,
          greenid_passportdvs_dob: data.dateOfBirth,
          greenid_passportdvs_number: data.identity.passport.number,
          greenid_passportdvs_gender: (data.gender || '')[0].toUpperCase(),
          greenid_passportdvs_tandc: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    if (data.identity.visa) {
      const idName = data.identity.visa.name || { }

      verificationData.push({
        visadvs: {
          greenid_visadvs_passport_number: data.identity.visa.passport_number || data.identity.passport.number,
          greenid_visadvs_givenname: idName.first || data.name.first,
          greenid_visadvs_middlename: idName.middle || data.name.middle || '',
          greenid_visadvs_surname: idName.last || data.name.last,
          greenid_visadvs_dob: data.dateOfBirth,
          greenid_visadvs_country_of_issue: data.identity.visa.country_of_issue,
          greenid_visadvs_tandc: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    if (data.identity.citizenship) {
      const idName = data.identity.citizenship.name || { }

      verificationData.push({
        citizenshipcertificatedvs: {
          greenid_citizenshipcertificatedvs_givenname: idName.first || data.name.first,
          greenid_citizenshipcertificatedvs_middlename: idName.middle || data.name.middle || '',
          greenid_citizenshipcertificatedvs_surname: idName.last || data.name.last,
          greenid_citizenshipcertificatedvs_dob: data.dateOfBirth,
          greenid_citizenshipcertificatedvs_stock_number: data.identity.citizenship.stock_number,
          greenid_citizenshipcertificatedvs_acquisition_date: data.identity.citizenship.acquisition_date,
          greenid_citizenshipcertificatedvs_tandc: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    if (data.identity.medicare) {
      verificationData.push({
        medicaredvs: {
          greenid_medicaredvs_number: data.identity.medicare.number,
          greenid_medicaredvs_individualReferenceNumber: data.identity.medicare.individualReferenceNumber,
          greenid_medicaredvs_expiry: data.identity.medicare.expiry,
          greenid_medicaredvs_nameOnCard: data.identity.medicare.nameOnCard,
          greenid_medicaredvs_nameLine2: data.identity.medicare.nameLine2,
          greenid_medicaredvs_nameLine3: data.identity.medicare.nameLine3,
          greenid_medicaredvs_nameLine4: data.identity.medicare.nameLine4,
          greenid_medicaredvs_cardColour: data.identity.medicare.cardColour,
          greenid_medicaredvs_dob: data.dateOfBirth,
          greenid_medicaredvs_tandc: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    if (data.identity.immigration) {
      const idName = data.identity.immigration.name || { }

      verificationData.push({
        immicarddvs: {
          greenid_immicarddvs_givenname: idName.first || data.name.first,
          greenid_immicarddvs_middlename: idName.middle || data.name.middle || '',
          greenid_immicarddvs_surname: idName.last || data.name.last,
          greenid_immicarddvs_dob: data.dateOfBirth,
          greenid_immicarddvs_card_number: data.identity.immigration.card_number,
          greenid_immicarddvs_tandc: data.identity.verificationConsent ? 'on' : null
        }
      })
    }

    return verificationData
  }
}

module.exports = VerificationMap
