const { mapState, mapCountry } = require('./util.js')

class XmlBuilder {
  constructor (account, password) {
    this.accountId = account;
    this.password = password;
  }

  registerVerification (data) {
    const date = data.dateOfBirth.match(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-](\d{4})$/)

    const xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/">\n' +
      '    <soapenv:Header/>\n' +
      '    <soapenv:Body>\n' +
      '        <dyn:registerVerification>\n' +
      '            <accountId>'+this.accountId+'</accountId>\n' +
      '            <password>'+this.password+'</password>\n' +
      '            <ruleId>default</ruleId>\n' +
      '            <name>\n' +
      '                <givenName>'+data.name.first+'</givenName>\n' +
      '                <middleNames>'+(data.name.middle||'')+'</middleNames>\n' +
      '                <surname>'+data.name.last+'</surname>\n' +
      '            </name>\n' +
      '            <email>'+data.contact.email+'</email>\n' +
      '            <currentResidentialAddress>\n' +
      '                <country>'+mapCountry(data.address.home.current.country)+'</country>\n' +
      '                <state>'+mapState(data.address.home.current.administrative_area_level_1)+'</state>\n' +
      '                <streetName>'+data.address.home.current.route+'</streetName>\n' +
      '                <streetNumber>'+data.address.home.current.street_number+'</streetNumber>\n' +
      '                <suburb>'+data.address.home.current.locality+'</suburb>\n' +
      '            </currentResidentialAddress>\n' +
      '            <dob>\n' +
      '                <day>'+date[1]+'</day>\n' +
      '                <month>'+date[2]+'</month>\n' +
      '                <year>'+date[3]+'</year>\n' +
      '            </dob>\n' +
      '            <generateVerificationToken>false</generateVerificationToken>\n' +
      '        </dyn:registerVerification>\n' +
      '    </soapenv:Body>\n' +
      '</soapenv:Envelope>';
    return xml
  }

  getSources (vId) {
    const xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/">' +
      '   <soapenv:Header/>' +
      '   <soapenv:Body>' +
      '      <dyn:getSources>' +
      '         <accountId>'+this.accountId+'</accountId>' +
      '         <password>'+this.password+'</password>' +
      '         <verificationId>'+vId+'</verificationId>' +
      '      </dyn:getSources>' +
      '   </soapenv:Body>' +
      '</soapenv:Envelope>';
    return xml
  }

  getFields (vId, source) {
    const xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/">' +
      '   <soapenv:Header/>' +
      '   <soapenv:Body>' +
      '      <dyn:getFields>' +
      '         <accountId>'+this.accountId+'</accountId>' +
      '         <password>'+this.password+'</password>' +
      '         <verificationId>'+vId+'</verificationId>' +
      '         <sourceId>'+source+'</sourceId>' +
      '      </dyn:getFields>' +
      '   </soapenv:Body>' +
      '</soapenv:Envelope>';
    return xml
  }
  
  setFields (vId, type, source) {
    let xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/">' +
      '   <soapenv:Header/>' +
      '   <soapenv:Body>' +
      '      <dyn:setFields>' +
      '         <accountId>'+this.accountId+'</accountId>' +
      '         <password>'+this.password+'</password>' +
      '         <verificationId>'+vId+'</verificationId>' +
      '         <sourceId>'+type+'</sourceId>' +
      '         <inputFields>';
      
    for (let key in source) {
      if (source.hasOwnProperty(key)) {
        xml+='            <input>' +
        '               <name>'+key+'</name>' +
        '               <value>'+source[key]+'</value>' +
        '            </input>'
      }
    }
    xml+=
      '         </inputFields>' +
      '      </dyn:setFields>' +
      '   </soapenv:Body>' +
      '</soapenv:Envelope>';
    return xml
  }
  
  getVerificationResult (vId) {
    const xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/">' +
      '   <soapenv:Header/>' +
      '   <soapenv:Body>' +
      '      <dyn:getVerificationResult>' +
      '         <accountId>'+this.accountId+'</accountId>' +
      '         <password>'+this.password+'</password>' +
      '         <verificationId>'+vId+'</verificationId>' +
      '      </dyn:getVerificationResult>' +
      '   </soapenv:Body>' +
      '</soapenv:Envelope>';
    return xml
  }
}

module.exports = XmlBuilder