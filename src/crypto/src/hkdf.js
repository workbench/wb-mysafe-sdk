const { hmac } = require('./core')
const { append } = require('./encoding')

class HKDF {
  constructor (ikm, salt = new Uint8Array()) {
    if (!(ikm instanceof Uint8Array)) throw new Error('Invaid ikm type, expected Uint8Array, got: ' + ikm + ' (' + (typeof ikm) + ')')
    this.prk = hmac(ikm, salt)
    this.t = new Uint8Array()
  }

  derive (length, info = new Uint8Array()) {
    let okm = new Uint8Array()
    for (let i = 0; i < Math.ceil(length / 32); i++) {
      this.t = hmac(append(this.t, info, new Uint8Array([1 + i])), this.prk)
      okm = append(okm, this.t)
    }

    return okm.subarray(0, length)
  }
}

HKDF.derive = (ikm, length = 32, info = new Uint8Array()) => new HKDF(ikm).derive(length, info)

module.exports = HKDF
