const managedQBox = require('./managed-qbox')
const { append, fasc, tasc, f64, t64, serialize, deserialize } = require('./encoding')
const core = require('./core')
const HKDF = require('./hkdf')

class Identity {
  constructor (data, password) {
    if (data && !(data instanceof Uint8Array) && JSON.stringify(data).indexOf('{') >= 0) {
      Object.assign(this, deserialize(data))
      return
    }

    if (!data) {
      this.secret = core.randomBytes(32)
    } else if (password) {
      this.secret = core.decrypt(f64(data), HKDF.derive(fasc(password)))
    } else {
      this.secret = f64(data, true)
    }

    const derive = name => HKDF.derive(this.secret, 32, fasc(name))
    const signKey = core.signKey(derive('sign'))
    const qboxKey = managedQBox.keyPair(derive('qbox'))

    this.hkdfKey = derive('hkdf')
    this.signKey = signKey.secretKey
    this.verifyKey = signKey.publicKey
    this.encryptKey = qboxKey.publicKey
    this.decryptKey = qboxKey.secretKey
    this.symmetricKey = derive('symmetric')
  }

  fingerprint () {
    if (this.verifyKey) this.vHash = core.hash(this.verifyKey)
    if (this.encryptKey) this.eHash = core.hash(this.encryptKey)

    if (!this.vHash || !this.eHash) throw new Error('Certificate does not support this operation')

    return t64(core.hash(append(this.vHash, this.eHash)).subarray(0, 16))
  }

  sign (data) {
    if (!this.signKey) throw new Error('Certificate does not support this operation')

    return t64(core.sign(fasc(serialize(data)), this.signKey))
  }

  verify (data, sig) {
    if (!this.verifyKey) throw new Error('Certificate does not support this operation')

    return core.signVerify(fasc(serialize(data)), f64(sig), this.verifyKey)
  }

  createCert (data) {
    if (!this.signKey) throw new Error('Certificate does not support this operation')

    return serialize({
      data,
      sig: t64(this.sign(data))
    })
  }

  openCert (cert) {
    if (!this.verifyKey) throw new Error('Certificate does not support this operation')

    cert = deserialize(cert)
    if (this.verify(cert.data, cert.sig)) return cert.data
    return null
  }

  encryptPublic (data, raw) {
    if (!this.encryptKey) throw new Error('Certificate does not support this operation')

    if (!raw) data = fasc(serialize(data))

    return managedQBox.encrypt(data, this.encryptKey)
  }

  decryptPrivate (ciphertext, raw) {
    if (!this.decryptKey) throw new Error('Certificate does not support this operation')

    const data = managedQBox.decrypt(ciphertext, this.decryptKey)

    return raw ? data : deserialize(tasc(data))
  }

  encryptSymmetric (data, raw) {
    if (!this.symmetricKey) throw new Error('Certificate does not support this operation')

    if (!raw) data = fasc(serialize(data))

    return t64(core.encrypt(data, this.symmetricKey))
  }

  decryptSymmetric (ciphertext, raw) {
    if (!this.symmetricKey) throw new Error('Certificate does not support this operation')

    const data = core.decrypt(f64(ciphertext), this.symmetricKey)

    return raw ? data : deserialize(tasc(data))
  }

  derive (keyName, length) {
    if (!this.hkdfKey) throw new Error('Certificate does not support this operation')

    return HKDF.derive(this.hkdfKey, length, fasc(keyName))
  }

  exportSecretKey (password) {
    if (!this.secret) throw new Error('Certificate does not support this operation')

    return t64(core.encrypt(this.secret, HKDF.derive(fasc(password))))
  }

  exportPublicKey (options) {
    const data = {}

    if (!options || options.verify) {
      data.verifyKey = this.verifyKey
    } else {
      data.vHash = core.hash(this.verifyKey)
    }

    if (!options || options.encrypt) {
      data.encryptKey = this.encryptKey
    } else {
      data.eHash = core.hash(this.encryptKey)
    }

    return serialize(data)
  }
}

module.exports = Identity
