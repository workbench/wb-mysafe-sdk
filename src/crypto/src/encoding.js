const stringify = require('json-stable-stringify')
const Uint8Encoding = require('./uint8-encoding')
const base64Prefix = 'b64u,'
const regexPrefix = 'regex,'

class Encoding extends Uint8Encoding {
  static serialize (obj, pretty) {
    return stringify(obj, {
      space: pretty ? 2 : undefined,
      replacer: (key, value) => {
        if (value instanceof Uint8Array) return base64Prefix + Uint8Encoding.toBase64(value)
        if (value instanceof RegExp) return (regexPrefix + value.toString())
        return value
      }
    })
  }

  static deserialize (json) {
    if (typeof json === 'object') json = Encoding.serialize(json)

    return JSON.parse(json, (key, value) => {
      if (typeof value === 'string') {
        if (value.startsWith(base64Prefix)) return Uint8Encoding.fromBase64(value.slice(base64Prefix.length))
        if (value.startsWith(regexPrefix)) {
          const m = value.slice(regexPrefix.length).match(/\/(.*)\/(.*)?/)
          return new RegExp(m[1], m[2] || '')
        }
      }
      return value
    })
  }

  static stringify () {
    return stringify.apply(null, arguments)
  }
}

module.exports = Encoding
