module.exports = (a, b) => {
  if (a.length === 0 || b.length === 0) return false
  if (a.length !== b.length) return false
  let result = 0
  for (let i = 0; i < a.length; i++) { result |= a.charCodeAt(i) ^ b.charCodeAt(i) }
  return (1 & ((result - 1) >>> 8)) - 1 === 0
}
