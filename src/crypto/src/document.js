const core = require('./core')
const { f64, t64 } = require('./encoding')

class Document {
  static encrypt (contents) {
    if (!(contents instanceof Uint8Array)) throw new Error('Unexpected type, use Uint8Array')

    const key = core.symmetricKey()
    const data = core.encrypt(contents, key)
    const hash = core.hash(data)

    return {
      key: t64(key),
      data: t64(data),
      hash: t64(hash)
    }
  }

  static decrypt (document) {
    const key = f64(document.key)
    const data = f64(document.data)
    const hash = f64(document.hash)

    if (!core.verify(hash, core.hash(data))) throw new Error('Document hash invalid')

    return core.decrypt(data, key)
  }
}

module.exports = Document
