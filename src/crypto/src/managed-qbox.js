const nacl = require('../lib/tweetnacl/nacl.js')
const ntruBox = require('./qbox').ntru(nacl)
const { append, f64, t64, split } = require('./encoding')

class ManagedQBox {
  static keyPair (seed) {
    return ntruBox.keyPair(seed)
  }
  static senderKeyPair (seed) {
    return ntruBox.keyPair.sendOnly(seed)
  }
  static encrypt (data, publicKey, sender) {
    const nonce = nacl.randomBytes(ntruBox.nonceLength)
    sender = sender || ManagedQBox.senderKeyPair()
    const { box, kem } = ntruBox(data, nonce, f64(publicKey), sender.secretKey)
    return t64(append(sender.publicKey, kem, nonce, box))
  }
  static decrypt (data, secretKey) {
    const parts = split(f64(data), [ ntruBox.publicSendOnlyKeyLength, ntruBox.kemLength, ntruBox.nonceLength ])
    return ntruBox.open(parts[3], parts[2], parts[0], f64(secretKey), parts[1])
  }
}

module.exports = ManagedQBox
