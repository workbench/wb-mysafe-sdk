/**
 * Encoding Utilities for Uint8Arrays
 * @module Uint8Encoding
 */
const unescRgx = /\\u([\d\w]{4})/gi
const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
const enc = alphabet.split('')
const dec = [ ]
for (let i = 0; i < alphabet.length; i++) dec[alphabet.charCodeAt(i)] = i
dec['+'.charCodeAt(0)] = dec['-'.charCodeAt(0)]
dec['/'.charCodeAt(0)] = dec['_'.charCodeAt(0)]

class Uint8Encoding {
  /**
   * Appends multiple Uint8Arrays together
   * @param {...Uint8Array} buffers - The list of buffers to append
   * @returns {Uint8Array}
   */
  static append (...buffers) {
    let length = 0
    let pos = 0
    for (let buf of buffers) length += buf.length
    const result = new Uint8Array(length)
    for (let buf of buffers) {
      result.set(buf, pos)
      pos += buf.length
    }
    return result
  }

  /**
   * Splits a buffer into chunks of the provided lengths, with any remaining data returned as an additional chunk
   * @param {Uint8Array} buffer - The source buffer
   * @param {...Integer} lengths - The length of each output buffer
   * @returns {Uint8Array[]}
   */
  static split (buffer, ...lengths) {
    if (lengths[0] instanceof Array) lengths = lengths[0]

    const result = [ ]
    for (let len of lengths) {
      result.push(buffer.subarray(0, len))
      buffer = buffer.subarray(len)
    }
    if (buffer.length) result.push(buffer)
    return result
  }

  /**
   * Encodes the input string in strict 8bit characters
   * @param {String} str - The source string
   * @returns {String}
   */
  static escape8bit (str) {
    return str.split('')
     .map(c => {
       const code = c.charCodeAt(0)
       if (code < 256) return c
       const hex = code.toString(16)
       return new Array(5 - hex.length).join('0') + hex
     })
     .join('')
  }

  /**
   * Decodes a string encoded with escape8bit
   * @param {String} str - The source string
   * @returns {String}
   */
  static unescape8bit (str) {
    return unescape(str.replace(unescRgx, (match, grp) =>
      String.fromCharCode(parseInt(grp, 16))
    ))
  }

  /**
   * Converts a Uint8Array to 8bit ASCII, optionally unescaping from 8bit ASCII - or simply returns the input if a string is provided
   * @param {String|Uint8Array} v - The source Uint8Array or string
   * @returns {String}
   */
  static toAscii (v, esc) {
    if (v instanceof Uint8Array) v = Array.from(v).map(c => String.fromCharCode(c)).join('')
    else if (typeof v !== 'string') throw new Error('Unexpected type, use string|Uint8Array')
    return esc
      ? Uint8Encoding.unescape8bit(v)
      : v
  }

  /**
   * Converts a string to a Uint8Array, optionally escaping to 8bit ASCII - or simply returns the input if a Uint8Array is provided
   * @param {String|Uint8Array} v - The source Uint8Array or string
   * @returns {Uint8Array}
   */
  static fromAscii (v, esc) {
    if (v instanceof Uint8Array) return v
    if (typeof v !== 'string') throw new Error('Unexpected type, use string|Uint8Array')
    if (esc) v = Uint8Encoding.escape8bit(v)
    return new Uint8Array(v.split('').map(c => c.charCodeAt(0)))
  }

  /**
   * Converts a Uint8Array to Base64 - or simply returns the input if a string is provided
   * @param {String|Uint8Array} v - The source Uint8Array or string
   * @returns {String}
   */
  static toBase64 (v) {
    if (typeof v === 'string') return v
    if (!(v instanceof Uint8Array)) throw new Error('Unexpected type, use string|Uint8Array')

    const p = (3 - v.byteLength % 3) % 3

    let r = ''
    for (let c = 0; c < v.length; c += 3) {
      let n = (v[c] << 16) + (v[c + 1] << 8) + (v[c + 2] || 0)
      r += enc[(n >>> 18) & 63] + enc[(n >>> 12) & 63] + enc[(n >>> 6) & 63] + enc[n & 63]
    }
    return r.substring(0, r.length - p)
  }

  /**
   * Converts a Base64 string to a Uint8Array - or simply returns the input if a Uint8Array is provided
   * @param {String|Uint8Array} v - The source Uint8Array or string
   * @returns {String}
   */
  static fromBase64 (v, strict) {
    if (v instanceof Uint8Array) return v
    if (typeof v !== 'string') throw new Error('Unexpected type, use string|Uint8Array')

    v = v.replace(/=/g, '')

    const p = (4 - v.length % 4) % 4
    const r = new Uint8Array((v.length + p) * 3 / 4 - p)
    let x = 0
    for (let c = 0; c < p; c++) v += 'A'

    let map

    if (strict) {
      map = code => {
        if (typeof dec[code] === 'undefined') throw new Error('Invalid base64 string')
        return dec[code]
      }
    } else {
      map = code => dec[code]
    }

    for (let c = 0; c < v.length; c += 4) {
      const n = (map(v.charCodeAt(c)) << 18) + (map(v.charCodeAt(c + 1)) << 12) + (map(v.charCodeAt(c + 2)) << 6) + map(v.charCodeAt(c + 3))
      r[x++] = (n >>> 16) & 255
      r[x++] = (n >>> 8) & 255
      r[x++] = n & 255
    }

    return r
  }

  /**
   * Returns the input in both Base64 and Uint8Array format
   * @param {String|Uint8Array|{arr:Uint8Array,b64:String}} v - The source Uint8Array or string
   * @returns {{arr:Uint8Array,b64:String}}
   */
  static dual64 (v) {
    if (typeof v === 'object' && v.arr && v.b64) return v
    return {
      arr: Uint8Encoding.fromBase64(v),
      b64: Uint8Encoding.toBase64(v)
    }
  }

  /**
   * Returns the input in both ASCII and Uint8Array format, optionally escaping/unescaping to/from 8bit format
   * @param {String|Uint8Array|{arr:Uint8Array,asc:String}} v - The source Uint8Array or string
   * @returns {{arr:Uint8Array,asc:String}}
   */
  static dualAsc (v, esc) {
    if (typeof v === 'object' && v.arr && v.asc) return v
    return {
      arr: Uint8Encoding.fromAscii(v, esc),
      asc: Uint8Encoding.toAscii(v, esc)
    }
  }

  /**
   * Alias for fromAscii
   */
  static fasc () { return Uint8Encoding.fromAscii.apply(null, arguments) }

  /**
   * Alias for toAscii
   */
  static tasc () { return Uint8Encoding.toAscii.apply(null, arguments) }

  /**
   * Alias for fromBase64
   */
  static f64 () { return Uint8Encoding.fromBase64.apply(null, arguments) }

  /**
   * Alias for toBase64
   */
  static t64 () { return Uint8Encoding.toBase64.apply(null, arguments) }
}

module.exports = Uint8Encoding
