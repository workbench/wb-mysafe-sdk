const core = require('./core')
const { fasc, t64, append, serialize, deserialize } = require('./encoding')

const HASH_LENGTH = 2

class Ratchet {
  constructor (options) {
    if (options instanceof Uint8Array) {
      this.key = options
    } else if (typeof options === 'object') {
      Object.assign(this, options)
    } else if (typeof options === 'string') {
      Object.assign(this, deserialize(options))
    }

    if (this.key) {
      if (!this.keyHash) this.keyHash = core.hash(this.key).subarray(0, HASH_LENGTH)
      if (typeof this.index === 'undefined') this.index = 0
    } else {
      throw Error('Ratchet must be initialised with Uint8Array(32)')
    }
  }

  getState () {
    return serialize({ keyHash: this.keyHash, key: this.key, index: this.index })
  }

  encrypt (message) {
    const ciphertext = core.ratchetEncrypt(fasc(message), this.key, this.index)

    this.key = core.ratchetKey(this.key)
    this.index++

    return t64(append(this.keyHash, ciphertext))
  }
}

module.exports = Ratchet
