const { fasc, tasc, f64, t64, dual64 } = require('./encoding')
const { encryptKeyed, openKeyed, decrypt, ratchetDecrypt, keyHash } = require('./core')

class KeyCollection {
  constructor (state) {
    Object.assign(this, state)
    if (!this.keys) this.keys = { }
  }

  addKey (key) {
    key = dual64(key)

    this.keys[key.b64] = t64(keyHash(key.arr))
  }

  getKeys (hash) {
    const b64 = t64(hash)
    const result = [ ]
    for (let keyb in this.keys) {
      if (this.keys[keyb] === b64) {
        result.push(f64(keyb))
      }
    }
    return result
  }

  encrypt (message, key) {
    return t64(encryptKeyed(fasc(message), f64(key)))
  }

  decrypt (keyedCiphertext, options) {
    options = options || { }
    const encoding = options.encoding || 'utf8'
    const ratcheted = options.ratcheted

    keyedCiphertext = f64(keyedCiphertext)

    let success = false
    let plaintext
    let [ keyHash, ciphertext ] = openKeyed(keyedCiphertext)

    for (let key of this.getKeys(keyHash)) {
      try {
        plaintext = ratcheted
          ? ratchetDecrypt(ciphertext, key)
          : decrypt(ciphertext, key)
      } catch (e) {
        console.error(e.stack || e)
        continue
      }
      if (plaintext !== null) {
        success = true
        break
      }
    }

    if (!success) return null

    switch (encoding) {
      case 'utf8': return tasc(plaintext)
      case 'base64': return t64(plaintext)
      case 'uint8': return plaintext
      default: return tasc(plaintext)
    }
  }
}

module.exports = KeyCollection
