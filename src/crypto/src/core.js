const nacl = require('../lib/tweetnacl/nacl.js')
const hmac = require('../lib/tweetnacl-auth/nacl-auth.js')
const { append, split } = require('./encoding')

const HMAC_KEY_LENGTH = 32
const HASH_LENGTH = 32
const KEYHASH_LENGTH = 2
const RATCHET_BYTES = new Uint8Array([114, 97, 116, 99, 104, 101, 116])// "ratchet"
const NO_NONCE = new Uint8Array(24)

class Core {
  static randomBytes (length) {
    return nacl.randomBytes(length)
  }

  static nonce () {
    return nacl.randomBytes(nacl.secretbox.nonceLength)
  }

  static symmetricKey () {
    return nacl.randomBytes(nacl.secretbox.keyLength)
  }

  static hmacKey () {
    return nacl.randomBytes(HMAC_KEY_LENGTH)
  }

  static hash (message, length) {
    return nacl.hash(message).subarray(0, length || HASH_LENGTH)
  }

  static hmac (message, key, length) {
    return hmac(message, key).subarray(0, length || HASH_LENGTH)
  }

  static verify (a, b) {
    return nacl.verify(a, b)
  }

  static sign (message, secretKey) {
    return nacl.sign.detached(message, secretKey)
  }

  static signVerify (message, signature, publicKey) {
    return nacl.sign.detached.verify(message, signature, publicKey)
  }

  static signKey (seed) {
    return seed
      ? nacl.sign.keyPair.fromSeed(seed)
      : nacl.sign.keyPair()
  }

  static encrypt (message, secretKey) {
    const nonce = Core.nonce()
    const box = nacl.secretbox(message, nonce, secretKey)
    return append(nonce, box)
  }

  static keyHash (key) {
    return Core.hash(key).subarray(0, KEYHASH_LENGTH)
  }

  static encryptKeyed (message, secretKey) {
    const hash = Core.keyHash(secretKey)
    return append(hash, Core.encrypt(message, secretKey))
  }

  static openKeyed (ciphertext) {
    return split(ciphertext, KEYHASH_LENGTH)
  }

  static decrypt (ciphertext, secretKey) {
    const parts = split(ciphertext, nacl.secretbox.nonceLength)
    const nonce = parts[0]
    const box = parts[1]
    return nacl.secretbox.open(box, nonce, secretKey)
  }

  static ratchetKey (key) {
    return Core.hash(append(key, RATCHET_BYTES), 32)
  }

  static ratchetEncrypt (message, key, index) {
    if (index > 0xFFFF) throw new Error('Max index (0xFFFF) exceeded')
    const box = nacl.secretbox(message, NO_NONCE, key)
    const indexBytes = new Uint8Array([ Math.floor(index / 0x100), index % 0x100 ])
    return append(indexBytes, box)
  }

  static ratchetDecrypt (ciphertext, key0) {
    let index = ciphertext[0] * 0x100 + ciphertext[1]
    const box = ciphertext.subarray(2)
    let key = key0
    while (index--) key = Core.ratchetKey(key)

    return nacl.secretbox.open(box, NO_NONCE, key)
  }
}

module.exports = Core
