const core = require('./core')
const { fasc, tasc, f64, t64, append, serialize, deserialize } = require('./encoding')

class Token {
  encode (value, secret) {
    const input = append(core.randomBytes(8), fasc(serialize(value)))
    const hmac = core.hmac(input, f64(secret), 8)

    return t64(append(hmac, input))
  }

  decode (token, secret) {
    token = f64(token)
    const hmac = token.subarray(0, 8)
    const input = token.subarray(8)

    if (!core.verify(hmac, core.hmac(input, f64(secret), 8))) return null

    return deserialize(tasc(input.subarray(8)))
  }
}

module.exports = new Token()
