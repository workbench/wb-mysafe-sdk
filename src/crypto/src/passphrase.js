const randomBytes = require('../lib/tweetnacl/nacl.js').randomBytes
const wordList = require('../lib/wordlist/eff-small-wordlist-2.js')
const bx = require('base-x')('123456')
const base6 = {
  encode: (bytes) => bx.encode(bytes),
  decode: (str) => new Uint8Array(bx.decode(str))
}

const index = { }
for (let key in wordList) index[wordList[key].slice(0, 3).toLowerCase()] = key

class passphrase {
  static generate (wordCount) {
    const bytes = randomBytes(wordCount * 2)

    let b6str = base6.encode(bytes)

    const words = []
    for (let i = 0; i < wordCount; i++) {
      words.push(wordList[b6str.slice(0, 4)])
      b6str = b6str.slice(4)
    }

    return words.join(' ')
  }

  static decode (phrase) {
    let b6str = ''
    const words = phrase.split(' ').map(w => w.slice(0, 3).toLowerCase())
    for (let word of words) {
      if (!index[word]) throw new Error('Invalid passphrase word: ' + word)
      b6str += index[word]
    }

    return base6.decode(b6str.replace(/\b1+/g, '') || '1')
  }

  static encode (bytes) {
    let b6str = base6.encode(bytes)
    const words = []

    while (b6str.length) {
      if (b6str.length < 4) b6str = (b6str + '111').slice(0, 4)
      words.push(wordList[b6str.slice(0, 4)])
      b6str = b6str.slice(4)
    }

    return words.join(' ')
  }
}

module.exports = passphrase
