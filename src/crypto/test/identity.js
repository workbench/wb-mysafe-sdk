const { Identity } = require('../crypto')
const expect = require('chai').expect

describe('Identity', () => {
  it('should return the same fingerprint regardless of the public key type', () => {
    const id = new Identity()
    const p1 = new Identity(id.exportPublicKey())
    const p2 = new Identity(id.exportPublicKey({ encrypt: true }))
    const p3 = new Identity(id.exportPublicKey({ verify: true }))
    const fingerprint = id.fingerprint()
    expect(fingerprint).to.equal(p1.fingerprint())
    expect(fingerprint).to.equal(p2.fingerprint())
    expect(fingerprint).to.equal(p3.fingerprint())
  })

  it('importing and exporting a public key should return the same value', () => {
    const publicKey = new Identity().exportPublicKey()
    expect(new Identity(publicKey).exportPublicKey()).to.equal(publicKey)
  })
})
