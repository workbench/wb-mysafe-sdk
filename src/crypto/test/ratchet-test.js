const expect = require('chai').expect
const { core, Ratchet, KeyCollection, encoding } = require('../crypto.js')
const { fasc, tasc } = encoding

describe('ratchet', () => {
  it('should successfully encrypt and decrypt a series of messages, regardless of decrypt order', () => {
    const messages = [
      'Hello',
      'And',
      'Goodbye'
    ]

    const key0 = core.symmetricKey()
    const ratchet = new Ratchet(key0)
    const ciphertext = messages.map(fasc).map(m => ratchet.encrypt(m))
    const kc = new KeyCollection()

    kc.addKey(key0)

    const plaintext = ciphertext.reverse().map(c => kc.decrypt(c, { ratcheted: true })).map(tasc).reverse()

    for (let i = 0; i < messages.length; i++) {
      expect(plaintext[i]).to.equal(messages[i])
    }
  })
})
