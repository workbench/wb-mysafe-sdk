const { f64, tasc } = require('../crypto').encoding
const expect = require('chai').expect

describe('encoding', () => {
  it('should decode a base64 string with trailing equals', () => {
    expect(tasc(f64('TG9yZW0gaXBzdW0=', true))).to.equal(tasc(f64('TG9yZW0gaXBzdW0', true)))
  })
  it('should accept non-url base64 encoding', () => {
    expect(tasc(f64('IjpMSz8+PE0/', true))).to.equal(tasc(f64('IjpMSz8-PE0_', true)))
  })
})
