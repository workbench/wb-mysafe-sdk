const { generate, encode, decode } = require('../crypto.js').passphrase
const expect = require('chai').expect

describe('passphrase', () => {
  const phrase = generate(3)
  it('should generate the provided number of words', () =>
    expect(phrase.split(' ').length).to.equal(3)
  )
  it('should decode to a Uin8Array', () =>
    expect(decode(phrase) instanceof Uint8Array).to.equal(true)
  )
  it('should decode and encode correctly', () =>
    expect(encode(decode(phrase))).to.equal(phrase)
  )
})
