const nacl = require('tweetnacl')
const ntruBox = require('../src/qbox').ntru(nacl)
const expect = require('chai').expect

const aliceSeed = nacl.randomBytes(32)
const alice = ntruBox.keyPair(aliceSeed)
const bob = ntruBox.keyPair(nacl.randomBytes(32))
const eve = ntruBox.keyPair.sendOnly(nacl.randomBytes(32))
const nonce = nacl.randomBytes(nacl.box.nonceLength)
const message = new Uint8Array([ 104, 101, 108, 108, 111 ])// "hello"

describe('qbox.ntruBox', () => {
  describe('keyPair', () => {
    it('should return the same key for the same seed', () => {
      const alice2 = ntruBox.keyPair(aliceSeed)
      expect(nacl.verify(
        alice.publicKey,
        alice2.publicKey
      )).to.equal(true)
      expect(nacl.verify(
        alice.secretKey,
        alice2.secretKey
      )).to.equal(true)
    })
  })
  describe('before', () => {
    it('should return the same sharedKey for both sender and receiver when full keys are used', () => {
      const { sharedKey, kem } = ntruBox.before(bob.publicKey, alice.secretKey)
      expect(nacl.verify(
        sharedKey,
        ntruBox.open.before(alice.publicKey, bob.secretKey, kem)
      )).to.equal(true)
    })
    it('should return the same sharedKey for both sender and receiver when a sendOnly key is used', () => {
      const { sharedKey, kem } = ntruBox.before(bob.publicKey, eve.secretKey)
      expect(nacl.verify(
        sharedKey,
        ntruBox.open.before(eve.publicKey, bob.secretKey, kem)
      )).to.equal(true)
    })
  })
  describe('box', () => {
    it('should encrypt and decrypt correctly when full keys are used', () => {
      const { box, kem } = ntruBox(message, nonce, bob.publicKey, alice.secretKey)

      expect(nacl.verify(
        ntruBox.open(box, nonce, alice.publicKey, bob.secretKey, kem),
        message
      )).to.equal(true)
    })
    it('should encrypt and decrypt correctly when a sendOnly key is used', () => {
      const { box, kem } = ntruBox(message, nonce, bob.publicKey, eve.secretKey)

      expect(nacl.verify(
        ntruBox.open(box, nonce, eve.publicKey, bob.secretKey, kem),
        message
      )).to.equal(true)
    })
  })
})
