if (typeof ntru !== 'undefined') { 
  //console.log('using global.ntru')
  module.exports = ntru 
} else { 
  //console.log('using require(\'ntru\')')
  //Use expression to prevent bundling
  module.exports = require((() => './ntru.min.js')()) 
}
