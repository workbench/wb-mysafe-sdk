if (typeof nacl !== 'undefined') {
  //console.log('using global.nacl')
  module.exports = nacl 
} else { 
  //console.log('using require(\'tweetnacl\')')
  //Use expression to prevent bundling
  module.exports = require((() => 'tweetnacl')()) 
}
