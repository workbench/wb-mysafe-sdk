/**
 * @class
 * @description Provides the ability to store temporary key/value
 */
class Cache {
    /**
     * Constructor
     */
    constructor () {
        this._storage = {}
    }

    /**
     * Adds data to temporary cache store
     * @param {String} key
     * @param {String} value
     * @param {Number} ttl - Time to live in milliseconds
     */
    set (key, value, ttl) {
        this._storage[key] = {
            expires: Date.now() + ttl,
            value: value
        }
    }

    /**
     * Gets data from cache by key
     * @param {String} key
     * @returns {Object|String}
     */
    get (key) {
        const data = this._storage[key]

        if (!data || Date.now() > data.expires) {
            this._storage[key] = null

            return null;
        }
        return this._storage[key].value
    }

  /**
   * Clears cache by key
   * @param {String} key
   */
    ivalidate(key) {
    this._storage[key] = null
    }
}

module.exports = Cache
