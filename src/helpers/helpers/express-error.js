module.exports = (res) => (err) => {
  if (err.response)
    res.status(err.response.status).send(err.response.data)
  else if (err.name === 'AssertionError')
    res.status(400).send({ err: err.message || err, message: 'Your request was rejected' })
  else
    res.status(500).send({ err: err.message || err, message: 'Your request could not be completed' })
  if (err.stack) console.error(err.stack)
  else console.trace(err)
}
