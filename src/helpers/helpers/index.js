const requireDirectory = require('require-directory')
const camelcase = require('camelcase')
module.exports = requireDirectory(module, {
  rename: name => camelcase(name)
})