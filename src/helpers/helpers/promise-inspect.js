var noop = a => a
module.exports = (label, func) => (obj) => (label ? console.log(label, (func || noop)(obj)) : console.log((func || noop)(obj))) || obj