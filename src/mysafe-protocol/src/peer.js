const { core, encoding, KeyCollection } = require('../../crypto')
const { f64, t64, fasc, stringify } = encoding
const { hmac, verify, symmetricKey } = core
const util = require('./util')
const Feed = require('./feed')

const ratchetPrefix = '[protected]'

class Peer {
  constructor (state) {
    Object.assign(this, state)

    if (!this.feeds) this.feeds = { }
    this.subscriptions = { }
    
    for (let feedId of Object.keys(this.feeds))
      if (!(this.feeds[feedId] instanceof Feed))
        this.feeds[feedId] = new Feed(this.feeds[feedId])
  }

  createFeed () {
    const feed = new Feed()

    this.feeds[feed.id] = feed

    return feed
  }

  getFeed (id) {
    if (id instanceof Uint8Array) id = util.keyId(id)

    return this.feeds[id] || this.subscriptions[id]
  }

  createSubscription (feedId, permissions) {
    const feed = this.feeds[feedId]
    const feedKey = feed.feedKey
    const subscriptionId = util.randomId()
    const permissionKeys = { }

    if (permissions instanceof Array) {
      const map = { }
      permissions.map(p => { map[p] = 'default' })
      permissions = map
    }

    Object.keys(permissions)
      .map(p => permissions[p])
      .filter((el, i, arr) => arr.indexOf(el) === i)
      .map(g => { permissionKeys[g] = symmetricKey() })

    const subscription = { id: subscriptionId, feedKey, permissions, permissionKeys }
      
    feed.subscribers[subscriptionId] = subscription

    return subscription
  }
  
  updateSubscription (feedId, subscriptionId, permissions) {
    const feed = this.feeds[feedId]
    const subscription = feed.subscribers[subscriptionId]
    const permissionKeys = subscription.permissionKeys
    
    if (permissions instanceof Array) {
      const map = { }
      permissions.map(p => { map[p] = 'default' })
      permissions = map
    }
    
    Object.assign(subscription.permissions, permissions)
    
    Object.keys(permissions)
      .map(p => permissions[p])
      .filter((el, i, arr) => arr.indexOf(el) === i)
      .filter(g => !permissionKeys[g])
      .map(g => { permissionKeys[g] = symmetricKey() })
      
    return subscription
  }

  getSubscriber (feedId, subscriptionId) {
    return this.feeds[feedId].subscribers[subscriptionId]
  }

  publish (feedId, data, options = { }) {
    const timestamp = Date.now()
    const feed = this.feeds[feedId]
    const ratchet = options.ratchet
    const ownData = options.ownData || { }
    const items = [ ]

    if (!feed) throw new Error('Unknown feed')
    if (typeof data !== 'object') throw new Error('Invalid type - expected object')

    const flatData = util.flatten(data)
    const flatOwnData = util.flatten(ownData)

    for (let key of Object.keys(flatData)) {
      const raw = flatData[key] || ''
      const value = ratchet && typeof raw === 'string' && !raw.startsWith(ratchetPrefix)
        ? ratchetPrefix + ratchet.encrypt(stringify(raw))
        : raw
      const strValue = stringify(value)
            
      const feedItem = {
        timestamp,
        key: feed.encrypt(key, feed.feedKey),
        keyHmac: util.hmacPath(key, feed.metaKey),
        value: { owner: feed.encrypt(strValue, feed.ownerKey) },
        dataHmac: t64(hmac(fasc(stringify({ key, value })), feed.metaKey)),
        integrity: t64(hmac(fasc(stringify({ key, value, timestamp })), feed.metaKey))
      }
      
      if (flatOwnData[key]) feedItem.ownData = feed.encrypt(stringify(flatOwnData[key]), feed.ownerKey)

      for (let id of Object.keys(feed.subscribers)) {
        //console.log(key, strValue, 'authorizing for', id)
        const sub = feed.subscribers[id]
        const permission = util.matchPermissions(Object.keys(sub.permissions), key)
        if (!permission) continue
        const permissionKey = sub.permissionKeys[sub.permissions[permission]]
        feedItem.value[id] = feed.encrypt(strValue, permissionKey)
      }

      items.push(feedItem)
    }

    return util.shuffle(items)
  }
  
  update (feedId, feedItems) {
    const feed = this.feeds[feedId]
    
    for (let feedItem of feedItems) {
      const key = feed.decrypt(feedItem.key)
      const strValue = feed.decrypt(feedItem.value.owner)
      
      for (let id of Object.keys(feed.subscribers)) {          
        const sub = feed.subscribers[id]
        const permission = util.matchPermissions(Object.keys(sub.permissions), key)
        if (!permission) continue
        //console.log(key, strValue, 'authorizing for', id)
        const permissionKey = sub.permissionKeys[sub.permissions[permission]]
        feedItem.value[id] = feed.encrypt(strValue, permissionKey)
      }
    }
    
    return feedItems
  }

  subscribe (subscription) {
    const { id, feedKey, permissionKeys } = subscription

    this.subscriptions[id] = Object.assign(new KeyCollection(), { subscriptionId: id, feedKey, permissionKeys }, util.expandFeedKey(feedKey))
    this.subscriptions[id].addKey(feedKey)

    Object.keys(permissionKeys).map(k => this.subscriptions[id].addKey(permissionKeys[k]))
  }

  createQuery (subscriptionId, keys) {
    return util.hmacPath(keys, this.getFeed(subscriptionId).metaKey)
  }

  decryptFeedState (feed, feedItems, options) {
    options = options || { }

    if (typeof feed === 'string' || feed instanceof Uint8Array) {
      feed = this.getFeed(feed)
      if (!feed) throw new Error('Unknown feed')
    }

    if (!options.sorted) feedItems.sort((a, b) => b.timestamp - a.timestamp)

    const id = feed.subscriptionId || 'owner'
    const data = { }
    const ownData = { }
    const dataHmacs = { }
    const timestamps = { }

    for (let feedItem of feedItems) {
      const key = feed.decrypt(feedItem.key)
      let isLatest = false
      let ownValue

      const dataHmac = f64(feedItem.dataHmac)

      if (!dataHmacs.hasOwnProperty(key)) {
        dataHmacs[key] = dataHmac
        isLatest = true
      }

      if (!feedItem.value[id]) {
        //console.error(key, 'not authorized for', id)
        continue
      }

      let value = JSON.parse(feed.decrypt(feedItem.value[id]))
      
      if (!value) {
        //console.error(key, 'decryption failed')
        continue
      }
      
      const integrity = hmac(fasc(stringify({ key, value, timestamp: feedItem.timestamp })), feed.metaKey)

      if (!verify(integrity, f64(feedItem.integrity))) {
        console.error(key, 'HMAC failed')
        continue
      }
      
      if (typeof value === 'string' && value.startsWith(ratchetPrefix) && id !== 'owner') {
        value = JSON.parse(feed.decrypt(value.split(ratchetPrefix)[1], { ratcheted: true }))
      }

      if (isLatest || verify(dataHmac, dataHmacs[key])) {
        //console.log(key, value, t64(dataHmac), true)
        data[key] = value
        timestamps[key] = feedItem.timestamp
        if (id === 'owner' && feedItem.ownData) ownData[key] = JSON.parse(feed.decrypt(feedItem.ownData))
      } else {
        //console.log(key, value, t64(dataHmac), false)
      }
    }

    return {
      data: util.unflatten(data),
      ownData: util.unflatten(ownData),
      timestamps: util.unflatten(timestamps)
    }
  }

  decryptFeedHistory (feed, feedItems, options) {
    options = options || { }

    if (typeof feed === 'string' || feed instanceof Uint8Array) {
      feed = this.getFeed(feed)
      if (!feed) throw new Error('Unknown feed')
    }

    if (!options.sorted) feedItems.sort((a, b) => b.timestamp - a.timestamp)

    const id = feed.subscriptionId || 'owner'
    const result = [ ]
    
    for (let feedItem of feedItems) {
      const key = feed.decrypt(feedItem.key)
      const timestamp = feedItem.timestamp
      const dataHmac = feedItem.dataHmac

      if (!feedItem.value[id]) {
        result.push({ key, value: undefined, timestamp, dataHmac })
        continue
      }

      let value = JSON.parse(feed.decrypt(feedItem.value[id]))
      const integrity = value && hmac(fasc(stringify({ key, value, timestamp: feedItem.timestamp })), feed.metaKey)
      
      if (typeof value === 'string' && value.startsWith(ratchetPrefix) && id !== 'owner') {
        value = JSON.parse(feed.decrypt(value.split(ratchetPrefix)[1], { ratcheted: true }))
      }
      
      const item = { key, value, timestamp, dataHmac }
      if (!value) {
        item.value = null
        item.error = 'decryption failed'
      } else if (!verify(integrity, f64(feedItem.integrity))) {
        item.value = null
        item.error = 'integrity check failed'
      } else {
        item.error = null
      }
      
      if (id === 'owner' && feedItem.ownData) item.ownData = JSON.parse(feed.decrypt(feedItem.ownData))
      
      result.push(item)
    }
    
    return result
  }
}

module.exports = Peer
