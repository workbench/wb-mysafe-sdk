const { core, encoding, HKDF } = require('../../crypto')
const { append, t64, f64, fasc } = encoding
const { hash, hmac, randomBytes } = core

const PATH_CHAR = '.'
const UNFLATTEN_REGEX = /\.?([^.[\]]+)|\[(\d+)\]/g

class Util {
  static randomId () {
    return t64(randomBytes(16))
  }

  static keyId (key) {
    const id = new Uint8Array([ 105, 100 ])// "id"
    return t64(hash(append(f64(key), id)).subarray(0, 32))
  }

  static expandFeedKey (feedKey) {
    const hkdf = new HKDF(feedKey)
    return {
      metaKey: hkdf.derive(32)
    }
  }

  static hmacPath (value, hmacKey) {
    if (value instanceof Array && typeof value[0] === 'string') return value.map(x => Util.hmacPath(x, hmacKey))

    return value && value.split(PATH_CHAR).map(p =>
      t64(hmac(fasc(p), hmacKey))
    ).join(PATH_CHAR)
  }

  static matchPath (path, value, exact = false) {
    const p = (path || '').split(PATH_CHAR).filter(v => v.length)
    const v = (value || '').split(PATH_CHAR).filter(v => v.length)

    if (exact && p.length !== v.length) return false

    for (let i in p) {
      if (p[i] !== v[i]) {
        return false
      }
    }

    return true
  }

  static matchPermissions (permissions, key) {
    for (let permission of permissions.sort().reverse()) {
      if (Util.matchPath(permission, key)) return permission
    }
    return null
  }

  static shuffle (array) {
    let currentIndex = array.length
    let temporaryValue
    let randomIndex
    const rnd = randomBytes(array.length)

    while (currentIndex !== 0) {
      randomIndex = Math.floor((rnd[currentIndex - 1] / 256) * currentIndex)
      currentIndex -= 1

      temporaryValue = array[currentIndex]
      array[currentIndex] = array[randomIndex]
      array[randomIndex] = temporaryValue
    }

    return array
  }

  static flatten (data) {
    const result = {}
    const recurse = (cur, prop) => {
      if (Object(cur) !== cur) {
        result[prop] = cur
      } else if (Array.isArray(cur)) {
        let l = cur.length
        for (let i = 0; i < l; i++) {
          if(cur[i]) {
            recurse(cur[i], prop + '[' + i + ']')
          }
        }
        if (l === 0) result[prop] = []
      } else {
        let isEmpty = true
        for (let p of Object.keys(cur)) {
          isEmpty = false
          recurse(cur[p], prop ? prop + '.' + p : p)
        }
        if (isEmpty && prop) result[prop] = {}
      }
    }
    recurse(data, '')
    return result
  }

  static unflatten (data) {
    'use strict'
    if (Object(data) !== data || Array.isArray(data)) return data
    const resultholder = {}
    for (var p in data) {
      let cur = resultholder
      let prop = ''
      let m = UNFLATTEN_REGEX.exec(p)
      while (m) {
        cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}))
        prop = m[2] || m[1]
        m = UNFLATTEN_REGEX.exec(p)
      }
      cur[prop] = data[p]
    }
    return resultholder[''] || resultholder
  }

  static toAssociatedObject(data) {
    'use strict'
    let result = Object.assign({}, data)

    this.traverse(result, (prop, parent, key)=> {
      if(Array.isArray(prop)) {
        parent[key] = prop.reduce((previous, current, i) => {
          previous[i] = current;

          return previous;
        }, {})
      }
    });

    return result;
  }

  static toArray(data) {
    let result = Object.assign({}, data)

    this.traverse(result, (prop, parent, key)=> {
      if(typeof prop === 'object' && Object.keys(prop)
          .every(propKey => !isNaN(+propKey))) {

        parent[key] = Object.keys(prop)
          .reduce((previous, current) => {
            previous[current] = prop[current]
            return previous
          }, [])
      }
    });

    return result;
  }

  static traverse(object, func) {
    Object.keys(object)
      .forEach(i => {
        func.call(this, object[i], object, i);
        if (object[i] !== null && typeof(object[i]) === 'object' && !Array.isArray(object[i])) {
          this.traverse(object[i], func);
        }
      })
  }
}

Util.PATH_CHAR = PATH_CHAR

module.exports = Util
