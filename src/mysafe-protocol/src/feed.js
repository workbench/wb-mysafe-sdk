const { core, KeyCollection } = require('../../crypto')
const { symmetricKey } = core
const util = require('./util')

class Feed extends KeyCollection {
  constructor (state) {
    super(state)
    
    if (!this.ownerKey) this.ownerKey = symmetricKey()
    if (!this.feedKey) this.feedKey = symmetricKey()
    if (!this.subscribers) this.subscribers = { }
      
    Object.assign(this, util.expandFeedKey(this.feedKey))

    this.id = util.keyId(this.feedKey)
    this.addKey(this.ownerKey)
    this.addKey(this.feedKey)
  }
}

module.exports = Feed
