module.exports = {
  Peer: require('./src/peer'),
  util: require('./src/util')
}

if (typeof window !== 'undefined') window.msp = module.exports
